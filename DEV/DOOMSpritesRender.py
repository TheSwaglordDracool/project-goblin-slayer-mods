import bpy

class PropVal(bpy.types.PropertyGroup):
    enumv = bpy.props.EnumProperty(
        name="my_enum_name:",
        description="my_enum_description",
        items=[ ("1.5", "A", ""),
                ("2.4", "B", ""),
                ("-5.2", "C", ""),
               ]
        )
        
class DOOMSpritesRender(bpy.types.Operator):
    """This is my simple operator"""
    bl_idname = "scene.doom_sprites_render"
    bl_label = "DOOM Sprites Render"
    
    def execute(self, context):
        scene = context.scene
        obj = context.active_object
        obj.rotation_euler = [0, 0, 0]
        return {"FINISHED"}


class DOOMAddonsPanel(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "DOOM Addons"
    bl_idname = "SCENE_PT_doom"

    def draw(self, context):
        obj = context.object
        scene = context.scene
        layout = self.layout
        
        # row 1 - active object
        row = layout.row()
        row.label(text="Active object: "+obj.name, icon='CUBE')
        
        # row 2 - render mode selection
        row = layout.row()
        row.label(text="Sprites render type")
        
        row = layout.row()
        row.prop(obj, "prop")
        #row.operator("scene.doom_sprites_render")

def register():
    bpy.utils.register_class(DOOMAddonsPanel)
    bpy.types.Object.prop =  bpy.props.EnumProperty(items=(('A', "1 angle", "Vanilla DOOM items and decorations representation"),
                         ('B',"8 angles", "Vanilla DOOM monsters and players rotations"),
                         ('C',"16 angles", "ZDOOM sprite max rotations")),
                         default='A', name="")


def unregister():
    del bpy.types.Scene.enumval
    bpy.utils.unregister_class(DOOMAddonsPanel)


if __name__ == "__main__":
    register()