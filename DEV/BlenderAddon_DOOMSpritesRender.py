bl_info = {
    "name": "DOOM Add-on",
    "description": "Based on p2or's template: https://blender.stackexchange.com/questions/57306/how-to-create-a-custom-ui/57332#57332",
    "author": "Rhaegar Stormbringer",
    "version": (0, 0, 3),
    "blender": (2, 80, 0),
    "location": "3D View > DOOM",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "category": "Development"
}


import bpy
import os
import math

from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       FloatVectorProperty,
                       EnumProperty,
                       PointerProperty,
                       )
from bpy.types import (Panel,
                       Menu,
                       Operator,
                       PropertyGroup,
                       )


# ------------------------------------------------------------------------
#    Scene Properties
# ------------------------------------------------------------------------

class MyProperties(PropertyGroup):

    my_string: StringProperty(
        name="",
        description=":",
        default="",
        maxlen=1024
        )

    my_path: StringProperty(
        name = "",
        description="Choose a directory:",
        default="",
        maxlen=1024,
        subtype='DIR_PATH'
        )

    my_enum: EnumProperty(
        name="",
        description="Apply Data to attribute.",
        items=[ ('A', "1 Angle", "Vanilla DOOM items and decorations representation"),
                ('B', "8 Angles", "Vanilla DOOM monsters and players rotations"),
                ('C', "16 Angles", "ZDOOM max sprites rotations"),
               ],
        default='A'
        )
    
    enum_axis: EnumProperty(
        name="",
        description="Axis to apply the rotation",
        items=[ ('0', "X Axis", ""),
                ('1', "Y Axis", ""),
                ('2', "Z Axis", ""),
              ],
        default='2'
        )

# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------

class WM_OT_HelloWorld(Operator):
    bl_label = "Render Sprites"
    bl_idname = "wm.doom_sprites"

    def execute(self, context):
        scene = context.scene
        obj = context.object
        mytool = scene.my_tool
        sprite_sufix_list = ['1', '9', '2', 'A', '3', 'B', '4', 'C', '5', 'D', '6', 'E', '7', 'F', '8', 'G']
        # reset rotation
        rotation_euler_save = obj.rotation_euler
        rotation_mode_save = obj.rotation_mode
        obj.rotation_mode = "XYZ"
        obj.rotation_euler = [0, 0, 0]
        rotation_axis = int(mytool.enum_axis)
        # set file format and path
        scene.render.image_settings.file_format = 'PNG'
        print("PATH: "+mytool.my_path+"\nNAME: "+mytool.my_string)
        filepath = os.path.join(mytool.my_path, mytool.my_string)
        # render sprite(s)
        if (mytool.my_enum == 'A'):
            print("A - 1 Angle (item / decoration)")
            scene.render.filepath = filepath+"0.png"
            obj.rotation_euler = [0, 0, 0]
            bpy.ops.render.render(write_still = 1)
        elif (mytool.my_enum == 'B'):
            print("B - 8 Angles (Vanilla DOOM monster / player)")
            for i in range(8):
                scene.render.filepath = filepath+sprite_sufix_list[i*2]+".png"
                bpy.ops.render.render(write_still = 1)
                obj.rotation_euler[rotation_axis] -= math.pi/4
        elif (mytool.my_enum == 'C'):
            print("C - 16 Angles (ZDOOM Sprites)")
            for i in range(16):
                scene.render.filepath = filepath+sprite_sufix_list[i]+".png"
                bpy.ops.render.render(write_still = 1)
                obj.rotation_euler[rotation_axis] -= math.pi/8
        # return to previous state
        obj.rotation_mode = rotation_mode_save
        obj.rotation_euler = rotation_euler_save
        return {'FINISHED'}

# ------------------------------------------------------------------------
#    Panel in Object Mode
# ------------------------------------------------------------------------

class OBJECT_PT_CustomPanel(Panel):
    bl_label = "DOOM Sprites Render"
    bl_idname = "OBJECT_PT_custom_panel"
    bl_space_type = "VIEW_3D"   
    bl_region_type = "UI"
    bl_category = "DOOM"
    bl_context = "objectmode"   


    @classmethod
    def poll(self,context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        obj = context.object
        mytool = scene.my_tool
        
        layout.row().label(text="Sprites Angles")
        layout.prop(mytool, "my_enum", text="")
        layout.row().label(text="Rotation Axis")
        layout.prop(mytool, "enum_axis", text="")
        layout.row().label(text="Active Object")
        layout.row().label(text=obj.name, icon='CUBE')
        layout.row().label(text="Sprites Base Name  (5 letters)")
        layout.prop(mytool, "my_string")
        layout.row().label(text="Output Directory")
        layout.prop(mytool, "my_path")
        layout.operator("wm.doom_sprites")
        layout.separator()

# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

classes = (
    MyProperties,
    WM_OT_HelloWorld,
    OBJECT_PT_CustomPanel
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    bpy.types.Scene.my_tool = PointerProperty(type=MyProperties)

def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
    del bpy.types.Scene.my_tool


if __name__ == "__main__":
    register()