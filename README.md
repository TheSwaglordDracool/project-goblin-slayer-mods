#             GOBLIN SLAYER -  DOOM MOD

Mod pour DOOM II inspiré de l'anime Goblin Slayer produit par White Fox. 


## Liens utiles
  - [doom wiki](https://doom.fandom.com/wiki/Entryway)
  - [forum zdoom](https://forum.zdoom.org/)
  - [liste des tuto](https://zdoom.org/wiki/Tutorials) du wiki de zdoom
  - [un tuto](http://slade.mancubus.net/index.php?page=wiki&wikipage=Part-1%3A-Creating-a-Map) sur comment créer une map basique avec Slade 3
  - [le tuto de Eevee](https://eev.ee/blog/2015/12/19/you-should-make-a-doom-level-part-1/) sur la création de maps avec Slade 3 : la suite du tuto précédent à bien des égards
  - [un tuto](http://gunlabs.blogspot.com/2011/01/tutorials.html) conseillé dans ce [thread](https://forum.zdoom.org/viewtopic.php?f=39&t=50060) et qui a l'air assez sérieux, peut être un bon point de départ
  - [lien d'installation](http://slade.mancubus.net/index.php?page=wiki&wikipage=Installation) de slade3 pour linux (pas d'installateur de base) : 
  - Tuto pour faire arme [part1](https://www.youtube.com/watch?v=5s9UPDIHR-Y)
  - Tuto pour faire arme [part2](https://www.youtube.com/watch?v=B7Fn9RIQmNo)
  - Le [google doc](https://docs.google.com/document/d/1Gdp0wsJCScASyKAXsG-aoJBxnuoUmnU15JL2w73j1bM/edit?usp=sharing) à utilisé pour noter tout ce qu'on trouve comme astuce sur slade ou pour le modding de doom en général
  - [L'OST](https://www.youtube.com/watch?v=ABl_HrUcAjU) de l'anime, pour se motiver !
  - [Freesound](https://freesound.org/) Un site pour récupérer plein de sons pour les armes

## PHASE 1 - FIRST BUILD

Les éléments qu'ils nous faut : 
  - **La 1ere Map :** Cave of Fate (référence au nom du 1er ep, ["The Fate of Particular Adventurer"](https://en.wikipedia.org/wiki/List_of_Goblin_Slayer_episodes)) 
  - **Armes Permanentes :** Epée courte et Bouclier, Arc
  - **Armes Temporaires :** Dague, Torche, Gourdin
  - **Miracles de la Prêtresse :** Protection, Heal (Holy Light si possible)
  - **Potions :** Potion de Soin si on a le temps (et si ça vaut le coup)
  - **Goblins :** Goblin de base, Babygoblin (mais sans leur capacité *Maturité*), Shaman (possiblement sans tout les sorts mais avec au moins *Foudre*)

Pour suivre l'avancement, se rendre dans la partie **Issues** du git

### Deadline pour la Phase 1 : Décembre 2019 - Mars 2020

## Cahier des charges

## Equipements

| Nom | Catégorie | Description | Capacités |
|-----|-------------|-----------| ----- |
| **Épée courte et Bouclier** | Arme Permanente | Armes de base : l'épée dans la main gauche (droite ?) et le bouclier dans l'autre | __Pour l'épée__ : Bouton gauche de la souris<br>**Frappe directe** Lors d'un clic simple, le joueur donne un coup d'épée rapide dont la direction d'attaque dépends de la direction de mouvement du joueur; Si il est immobile, un coup en diagonale de base est réalisé<br>**Frappe puissante** Le joueur peut maintenir le bouton de la souris enfoncé pour charger son coup avant son attaque, entrainant plus de dégâts; L'attaque à toujours lieu selon la direction du mouvement<br><br>__Pour le bouclier__ : Bouton droit de la souris<br>**Blocage** Cliquer et maintenir le bouton de la souris permet de lever le bouclier afin de bloquer les attaques ennemis : le blocage n'annule pas les dégats mais les réduits (?); Il est de plus possible en sprintant de charger les ennemis et de les repousser jusqu'à un mur, et de leur infliger des dégâts en les écrasant contre<br>**Coup de bouclier** Un clic simple permet de donner un coup de bouclier devant soit qui étourdi les ennemis / les projette en arrière et annule leur attaque |
| **Arc** | Arme Permanente | Arc elfique, déblocable à partir du lvl X (?), qui permet de tirer des flèches normales et élémentaires (feu, glace, poisons ...) : le changement de munitions se fait avec la touche de sélection d'arme (en fait il y a un arc par type de flèches dans les données du jeu) | **Zoom** Le bouton droit permet de zoomer afin de tirer plus précisément<br>**Tendre la corde** Rester appuyer avec le bouton gauche permet de tendre la corde de l'arc, augmentant la puissance (dégâts + vitesse) de la flèche lors du tir. Il y a 3 paliers de puissance (tir normal, tir puissant, tir brutal), et un dernier palier où la flèche part automatiquement avec une grosse dispersion |
| **Cimeterre** | Arme Permanente | Cimeterre d'homme-lézard, déblocable à partir du lvl X (?), plus puissant que l'épée courte, mais demandant les deux mains pour être utilisé, empêchant ainsi le blocage avec le bouclier. La longueur de la lame fait qu'il est également difficile de l'utiliser dans les espaces étroits | **Frappe directe** Lors d'un clic simple, le joueur donne un coup d'épée rapide dont la direction d'attaque dépends de la direction de mouvement du joueur; Si il est immobile, un coup en diagonale de base est réalisé<br>**Frappe Tranche-maille** Le bouton droit de la souris permet de réaliser une attaque plus lente, mais pouvant briser les armures et les boucliers ennemis |
| **Fronde** | Arme Permanente | Fronde naine, déblocable à partir du lvl X (?), permettant de lancer rapidement des pierres à la face des goblins qui, si elles ne sont pas assez puissantes pour les tuer, permet de les étourdir quelques instants | **Pierre étourdissante** L'attaque normale permet de lancer une pierre qui stun les goblins. Les Hobgoblins, Champion goblins et les Boss sont cependant immunisés à cet effet<br>**Mitraille** Le tir secondaire permet de projeter une volée de petites pierres, qui si elles ne stun pas peuvent blesser plusieurs ennemis en face du joueur |
| **Dague** | Arme Temporaire | Petite dague lootable sur les goblins, pouvant être enduite de poison ou de pétrole pour ajouter un effet supplémentaire  | **Attaque pernicieuse** Une attaque rapide avec peu d'allonge. La 3eme attaque consécutive provoque obligatoirement un coup critique<br>**Lancer** Lance la dague afin d'infliger des dégâts à distance |
| **Lance** | Arme Temporaire | Lance lootable sur certains goblins mieux équipés que la moyenne | **Frappe large** Attaque normale, avec une allonge supplémentaire et la capacité d'infliger des dégâts à plusieurs ennemis à la fois (?)<br>**Lancer** Lance la lance afin d'infliger des dégâts à distance |
| **Gourdin** | Arme Temporaire | Lourd morceaux de bois que certains goblins brutaux utilisent comme arme de prédilection | **Frappe lourde** Attaque similaire à l'attaque de l'épée courte, mais infligeant des dégâts inversement proportionnels à la taille de l'ennemi. De plus l'attaque repousse les ennemis en arrière<br>**Frappe meurtrière** L'attaque secondaire inflige un coup critique en plus de stun l'adversaire, mais le gourdin est détruit dans l'opération |
| **Torche** | Arme Temporaire | En plus d'être un outil indispensable pour l'exploration, la torche peut également être une arme de fortune dévastatrice pour le corps et l'esprit si mise dans les bonnes mains | **Frappe directe** Lors d'un clic simple, le joueur donne un coup d'épée rapide dont la direction d'attaque dépends de la direction de mouvement du joueur; Si il est immobile, un coup en diagonale de base est réalisé. De plus, l'ennemi à une chance de prendre feu suite à l'attaque<br>**Lancer** Lance la lance afin d'infliger des dégâts à distance et enflamme une petite zone au niveau du point d'impact |
| **Bombe** | Consommable | Fabriqué à l'aide de poudre noire et de divers composants, les bombes sont des outils dévastateurs mais qui doivent être utilisés avec précaution | Les bombes sont des armes jetables qui explosent à l'impact. Deux types de lancer sont possible : le **Lancer Direct** et le **Lancer en Cloche**. Le Lancer en cloche envoi la bombe plus haut et la fait chuter plus lentement. Une bombe qui se fait tirer dessus pendant son vol explose et produit une zone d'effet plus grande au sol que si elle avait explosé par elle-même. Il existe différents types de bombes, chacun produisant un effet différent :<br><br>**Bombe incendiaire** Bombe de feu, enflammant la zone d'impact et tout ce qui s'y trouve<br><br>**Bombe chimique** Bombe empoisonnant tout ennemi dans la zone d'impact 
| **Sort** | Magie | Les sorts permettent d'invoquer la magie sous plusieurs formes que ce soit pour l'attaque, la défense, le soutien ou encore la création d'handicap pour l'ennemi. Ils apparaissent sous forme de parchemins avec des écritures à réciter pour invoquer la magie. Ils sont cependant à usage limité et doivent être trouver dans la map pour pouvoir les utiliser contrairement aux miracles. | **Parchemin du portail** Le seul vrai sort utiliser par Goblin Slayer au cours de l'anime. Il permet d'invoquer un portail n'importe où dans le monde et le relie au parchemin pour soit rejoindre le lieu en question, soit invoquer quelque chose en provenance de ce lieu. Dans le jeu on pourrait ramener le Goblin Slayer en lieu sûr où invoquer de l'eau comme il le fait contre l'ogre<br><br> **Fear** Sort d'attaque: Invoque une magie de type illusion qui rend les goblins confus et appeurés. Les goblins perdent toutes capacités défensive dans cet état<br><br> **Lame enflammée** Sort d'attaque: Ajoute un manteau de flamme sur la lame de l'épée du Goblin Slayer pour en augmenter les dégats<br><br> **Boule de feu** Sort d'attaque: Invoque une énorme boule de feu rasant tout sur son passage<br><br> **Mur de pierre** Sort défensif: Invoque un mur de pierre pour se protéger des attaques. Les goblins peuvent quand même contourner le mur. Peut aussi servir à faire du controle de zone<br><br> **Déflection** Sort défensif: Utilise la magie télékinétique pour renvoyer n'importe quel projectile à son lanceur d'origine. Peut aussi renvoyer les sorts adverses. <br><br> **Manteau de lumière** Sort défensif: Recouvre le Goblin Slayer d'un manteau protecteur le protégeant de 2-3 coups maximum avan de céder<br><br>**Cure** Sort de soin. Permet de soigner tous les poisons |
| **Miracle** | Magie divine | Les miracles sont de la magie dont leur utilisation a été accordé par les dieux. Ils sont principalement destinés aux soins et à la protection, ainsi qu'à la défense. Il est possible d'invoquer les miracles à travers des incantations ou des prières envers les dieux. Les miracles ne peuvent être utilisés que X fois par map et par divinité. Pour apprendre de nouveaux miracles, le Goblin Slayer doit sauver des prêtresses au cours des niveaux | **Sorts de Lumiere**<br>**Protection** Créer une barrière magique horizontale bloquant les mouvements et les attaques des ennemis. La barrière peut encaisser X points de dégats avant de se briser. Le Goblin Slayer (et ses attaques) peut la traverser<br>**Holy Light** Une lumière vive illumine la pièce, aveuglant tout les ennemis s'y trouvant pour T frames et éclairant la salle pour Y frames<br>**Heal** Un sort de soin rendant immédiatement X points de vie au Goblin Slayer<br><br>**Sort de Terre**<br>**Stone Blast** Une volée de débris partent en cône devant le Goblin Slayer, blessant légèrement et assomant les goblins pendant T frames
| **Potion** | Consommable | Les potions ont différents effets, comme soigner ou offrir une protection temporaire. Elles sont moins efficaces que les sorts mais sont utilisables sans restriction, pour peu que le joueur se soit procuper un stock suffisant, par le loot ou par le craft | Plusieurs potions sont disponibles, chacune necessitant des composants différents pour être crafté. Elles peuvent également être trouvé dans les maps ou looté (rarement) sur des goblins<br><br>**Potion de soin** Rends X points de vie au joueur [Fiole vide - ??? - ???] |
| **Piège** | ? | ? |


## Ennemis

Il y a dans DOOM 2 en tout **20** monstres en comptant tout les monstres du premier DOOM (10) et les boss (2 dans DOOM 1, 2 dans DOOM 2).
Comme Goblin Slayer n'est pas très généreux au niveau de la diversité des ennemis, deux solutions complémentaires peuvent être mettre en place :
- Diviser selon l'arme les Goblins : ainsi, les Goblins normaux possèdent seulement des armes de corps à corps, et un autre type de Goblins est créer pour les archers, les lanciers, etc
- Bien différencier les Goblins que ce soit au niveau du charac design que des capacités, et pour cela, s'inspirer de Dungeon & Dragons (D&D), dont Goblin Slayer s'inspire également. L'important étant alors de sélectionner des types de Goblins qui seraient intéressant et pertinent avec Goblin Slayer (par exemple éviter les Goblins des neiges)

| Nom            | Description         | Capacités                 | Attributs                  |
|----------------|---------------------|---------------------------|----------------------------|
| **Goblin**     | Ennemi de base      | **Attaque** Une attaque très basique. Le goblin s'avance vers le Goblin Slayer afin de lui asséner des dégats avec sa massue | X points de vie<br>Arme1 : gourdin<br>Arme2 : Aucune |
| **Scoutgoblin** | Goblin posté aux points clés des nids, il fait office de sentinelle | **Ralliement** Si en infériorité numérique, essaye de crier afin d'invoquer une horde de Goblins | X points de vie<br>Arme1 : Lance<br>Arme2 : Aucune
| **Babygoblin** | Bébé goblin, que l'on aperçoit à plusieurs reprise au cours de l'anime | **Maturité** Si possible, essayer de faire que si le joueur ne les tue pas, ils reviennent dans le niveau suivant, adulte et sous une forme adapté pour contrer le style de jeu du joueur | X points de vie<br>Arme1 : Aucune<br>Arme2 : Aucune<br>Comportement : Fuit lorsque le joueur est proche|
| **Snotling** | Petit goblin stupide, saute des fois sur le Goblin Slayer pour le poignarder [comme dans l'anime](https://youtu.be/Fy9no0fRUSQ?t=24) | **Saut au poignard** Saute sur le Goblin Slayer et le poignarde tant que ce dernier n'a pas tué le goblin en le tapant avec **[MOUSE-2]** | X points de vie<br>Arme1 : Poignard<br>Arme2 : Aucune |
| **Thoulgoblin** | Goblin lâche et vicieux, à l'air de petit troll, attaquant à l'aide d'arcs rudimentaires | **Flèche élémentaire** Tire une flèche d'un certain type (poison, feu, etc) qui inflige un debuff au joueur | X points de vie<br>Arme1 : Arc court<br>Arme2 : Aucune |
| **Norkgoblin** | Goblin mieux équipé que la moyenne, portant des armures en métal récupérées et armés de lances ou de javeline | **Lancer de javeline** Jette son arme et sort une épée (et un bouclier ?) pour la suite du combat | X points de vie<br>Arme1 : Lance<br>Arme2 : Epee + bouclier |
| **Varagoblin** | Entre le Goblin de base et l'Hobgoblin, ce Goblin musclé charge et tue ses ennemis à l'aide de lames métalliques | **Charge** Accélération soudaine suivit d'une attaque plus puissante | X points de vie<br>Arme1 : Deux lames courbes<br>Arme2 : Aucune |
| **Goblinriders** | Goblin chevauchant des loups apprivoisés, vu dans l'épisode 11 de l'anime | **Lancer de javeline** De loin, chevauche son loup en cercle autour du Goblin Slayer et lui lance des javelines<br>**Charge sanglante** A partir d'une certaine distance, saute du loup, et ce dernier charge le Goblin Slayer pour le mordre et le mettre à terre, l'empêchant d'utiliser ses armes tant qu'il n'est pas tué (comme pour le Snotling) et le faisant saigner, pendant que le Goblinrider continue de tirer ses javelines<br>**Charge empaleuse** Finalement, une fois seul, tente de charger le Goblin Slayer et de le fixer au sol en le plantant de sa javeline pour le ruer de coups jusqu'à ce qu'un des deux meurts. Si la charge rate, il attaque au corps à corps normalement<br><br>*Note sur le fonctionnement du mob :* Le total des points de vie est X+Y. A partir de Y pv enlevés, le Loup meurt et il ne reste que le Goblinrider, qui est stun pendant T frames, avec **T = (X+Y) - (nb dégats subi - Y)**<br>Je ne pense pas que ça soit possible, mais on peut essayer de faire une hitboxe plus "intelligente", aka si le joueur tire sur le rider et pas le Loup, il peut mourir avant le Loup - mais je pense pas que ça soit possible| X points de vie (Goblinrider)<br>Y points de vie (Loup)<br>Arme1 : Javeline<br>Arme2 : Aucune |
| **Hobgoblin**  | Goblin plus grand et plus fort, mais pas plus intelligent que ces congénères, il utilise son corps afin de broyer ses ennemis | **Poing écraseur**Tente d'écraser le Goblin Slayer, infligeant de très lourd dégats et le stunnant au sol pendant quelques frames<br>**Poigne broyeuse**Tente de saisir le Goblin Slayer, et de le broyer entre ses mains, avant de le projeter au loin, infligeant des dégats supplémentaires<br>**Wazzup !** Prends de l'élan, et saute afin d'écraser le Goblin Slayer depuis les airs, faisant d'énorme dégats (quasi OS assuré) | X points de vie<br>Arme1 : Poings<br>Arme2 : Aucune |
| **Shaman**     | Goblin pouvant utiliser la magie, gouverne des nids de goblins | **Foudre** Un rayon qui part en ligne droite et stun la 1ere cible touché <br>**Bouclier de Foudre / Feu** 3 boules entourent le shaman puis s'éloignent dans 3 directions différentes, infligeant un debuff à la 1ere cible touché <br>**Onde de Flamme** Une explosion de feu se propage autour du shaman, brulant les cibles proches et les projettant en arrière  <br>**Arme élémentaire** Ajoute un élément à une arme d'un gobelin allié (feu ou foudre) <br>**Totem** Le shaman pose un totem au sol, qui attaque le joueur en lançant des boules de feu ou de foudre <br>... <br>peut être une bonne idée de faire que plus la difficulté de la partie est haute, plus le shaman a de sorts / peut les utiliser plus souvent ? (inspiration pour les sorts : WoW) | X points de vie<br>Arme1 : Baton<br>Arme2 : Aucune |
| **Champion**   | L'équivalent des Norkgoblins chez les géants : Force et taille démesurées, il porte une armure de cuir protegeant son torse, et est armé d'objets insolites mais extrêment dangereux entre ses mains pour une cible de plus petite taille, comme un humain | **Come over here !** Lance son ancre en ligne droite, repoussant le Goblin Slayer si il est touché, puis la ramène à sa main grâce à la chaine<br>**Barrage** Fait tournoyer son ancre autour de lui, empêchant quiconque de s'approcher sous peine de se faire violemment repousser en arrière<br>**Mordhau** Retourne son "épée" et frappe, plaquant violemment au sol quiconque se retrouve sous le coup<br>**Lancer rageur** Lance son "épée" à travers la pièce, et charge avec l'ancre dans les deux mains. Durant ce mode, tente d'écraser le Goblin Slayer par succession de 3 coups à l'ancre<br>**Choc et chaine** Lance son ancre verticalement, qui retombe en produisant une onde de choc au poing d'impact, puis tire sur la chaine pour faire réaliser à l'ancre un arc de cercle de 180°, balayant tout sur son passage | X points de vie<br>Arme1 : Rame en métal<br>Arme2 : Ancre |

En plus des ennemis normaux du Goblin Slayer, ce dernier fait la rencontre de **4** ennemis uniques au cours de l'anime.
On peut donc se servir de ces monstres en tant que **Boss** du Mod.

| Nom            | Description         | Capacités                 | Attributs                  |
|----------------|---------------------|---------------------------|----------------------------|
| **Ogre**     | Ogre directement inspiré par [l'Ogre Mage de D&D](http://www.d20srd.org/srd/monsters/ogreMage.htm), que le Goblin Slayer rencontre dans [l'épisode 4](https://www.youtube.com/watch?v=eKKlfu-7yoM) | **Régénération**Dans l'anime et les règles de D&D, un ogre est capable de régénérer n'importe quel blessure mineure, de recoudre ses membres sectionnés en les appuyant sur la plaie pendant 1 minute, et de survivre à un coup fatal (tête tranchée ou organnes vitaux détruits) de la même manière en 10 minutes. Il ne peut cependant pas faire repousser ses membres perdus. En jeu, ce pouvoir de régénération est représenté par différentes phases : d'un nombre variant de 1 (en facile) à 3 (en difficulté max), chaque nouvelle phase après la 1ere arrive une fois la barre de vie du Boss vide : certains pouvoirs sont alors légèrement modifiés, et il en acquièrent un nouveau (décris plus bas)<br>**Boule de Feu** Après 1 seconde de cast, projette une boule de feu qui se déplace en ligne droite et provoque une explosion de feu à l'impact<br>*Phase 2* - En plus de l'explosion, des traits de feu partent dans les 8 directions cardinales<br>**Immolation** Fait sortir une immense colonne de feu du sol. Un marqueur permet de voir quelques instants avant qu'elle ne sorte l'emplacement de la colonne<br>*Phase 3* - Au lieu d'une colonne de flamme, le sort créer une succession de colonne sur une ligne horizontale, formant un mur de feu<br>**Cercle démoniaque** Place un pentagramme vert au sol, qui ralentit le Goblin Slayer pendant T frames si il marche dedans - le pentagramme est détruit dans l'opération<br>*Phase 2* - Le Goblin Slayer est maintenant figé pendant X secondes si il marche dedans<br>**Frappe des Ténèbres** Charge son arme avec de l'énergie des ombres, et saute en l'air pour frapper le Goblin Slayer  de sa lame<br>*Phase 3* - Le sol se craquelle en zigzag devant le point d'impact sous l'effet des Ténèbres, infligeant des dégats très important à quiconque se trouve sur la trajectoire<br>**Portail démoniaque (Phase 2)** Place un pentagramme violet au sol, sur lequel l'Ogre se téléporte au bout d'un certain temps - le pentagramme est détruit dans l'opération<br>**Pluie de feu (Phase 3)** Des météores tombent du ciel aléatoirement dans la pièce, explosant à l'impact et enflammant tout dans la zone d'effet | X points de vie<br>Arme1 : Lame démoniaque courbe<br>Arme2 : Aucune |
| **Eyeball**  | Enorme oeil flottant, inspiré par le [Beholder de D&D](https://fr.wikipedia.org/wiki/Tyrann%C5%93il), que le Goblin Slayer rencontre dans [l'épisode 8]() | **Desintegration** Un rayon laser sortant de l'oeil de l'Eyeball <br>**Dispel** Un sort d'annulation<br>**Capacité 3** A définir<br>**Capacité 4** A définir<br>**Capacité 5** A définir | ? |
| **Goblin Lord** | Ennemi final de l'anime, que l'on voit dans les [épisodes 11 et 12](https://www.youtube.com/watch?v=VGuw8gSMpYg). C'est le seul goblin de l'anime que l'on entends parler, il est armé d'une hache à deux mains, et porte un grand manteau à col fourré ainsi qu'une couronne sur la tête et une armure de cuir. | <br>**Capacité 1** A définir<br>**Capacité 2** A définir<br>**Capacité 3** A définir<br>**Capacité 4** A définir<br>**Capacité 5** A définir<br>**Capacité 6 ?** A définir ? | ? |
| **Swamp Dragon** | Un énorme Alligator albinos que le Goblin Slayer rencontre dans [l'épisode 6](https://www.youtube.com/watch?v=b5WStXS3QWU). Il s'en sert pour tuer des goblins voulant envahir la ville en passant par les égouts, mais ne le combat pas directement (Boss bonus ?) | ? | ? |


## Levels
L'anime fait 12 épisodes, ça serait du coup bien d'arriver à faire 12 niveaux. Cela dit c'est compliqué car tout les épisodes ne prennent pas place dans des lieux très intéressant pour un jeu vidéo.
Le synopsis de chaque épisode est le suivant :
- **Episode 1** : Un groupe de noobs veulent nettoyer une grotte de goblins et se font défoncer. La prêtresse du groupe se fait sauver par le Goblin Slayer.
- **Episode 2** : [Episode filler] présentation de l'univers : la ferme où le Goblin Slayer vit, la guilde où il prends ses quêtes. A la fin on le voit mettre le feu à une grotte avec la prêtresse mais sans plus.
- **Episode 3** : [Episode filler] Le nain, l'elfe et l'homme-lézard rencontrent le Goblin Slayer. Ils partent ensembles faire une mission.
- **Episode 4** : Le groupe rentre dans le Temple Haut Elfe, combat des goblins puis l'Ogre, et finissent pas le tuer.
- **Episode 5** : [Episode filler] Le goblin slayer aide à la ferme, le goblin slayer aide à la guilde, introduction de perso secondaires (guts, les 2 newbies qui tuent des rats dans les égouts, la love interest de la Questgiver etc)
- **Episode 6** : Le groupe se rendent à la ville thermale, rencontre la grande prêtresse, et accepte d'explorer les égouts. Ils empêchent des goblins en bateaux de passer, puis remonte à la surface.
- **Episode 7** : Retour dans les égouts, chercher qui fait passer des goblins. Ils tombent dans un piège, et doivent combattre des goblins et un champion. Ils gagnent, mais Goblin Slayer et la prêtresse tombent inconscient à la fin de l'épisode.
- **Episode 8** : [Episode filler] Le Goblin Slayer se soigne, fait des courses, et répare son équipement, puis retour dans les égouts avec le groupe. Ils rencontrent l'Eyeball et le tue.
- **Episode 9** : Suite directe de l'épisode précédent, dernier combat dans les égouts, retour à la surface, discussion avec la grande prêtresse, retour à la ville de la guilde.
- **Episode 10** : [Episode filler] Développement des persos secondaires. A la fin, le Goblin Slayer effectue sa patrouille quotidienne autour de la ferme, et remarque des dizaines de traces de goblin.
- **Episode 11** : Afin d'endiguer l'invasion goblin, le Goblin Slayer se rend à la guilde pour demander l'aide de tout les aventuriers, qu'il obtient. La  seconde partie de l'épisode se concentre sur la défense de la ferme par les aventuriers.
- **Episode 12** : Suite de la défense de la ferme, les gros bills aventuriers combattent les gros bills goblins, le Goblin Slayer et la prêtresse combattent le Goblin Lord. Ils gagnent, fête à la  guilde.

En tout le Goblin Slayer visite **7 lieux** au cours de son aventure : La 1ere grotte, la ferme, la guilde, le tronc enflammé, le temple haut elfe, la ville thermale, les égouts (en 3 fois).
Si on conserve la division des égouts en 3 parties, ça fait **9 niveaux potentiels**. Si on réuni la ferme et la guilde en 1 niveaux pour servir de hub, on redescend à **8 niveaux**. On peut donc soit réutiliser le hub plusieurs fois - avec une variante genre ville thermale pour changer à un moment, soit trouver de nouveaux niveaux, par exemple le passage où le Goblin Slayer va détruire le nid du Goblin Lord pendant la bataille de la ferme.
La structure du mod concernant les niveaux pourrait donc à peu près être comme ça :
- **Level 1** 1ere grotte
- **Level 2** Hub (Ferme / Guilde)
- **Level 3** Tronc Enflammé
- **Level 4** Temple Haut Elfe
- **Level 5** Hub (Ferme / Guilde)
- **Level 6** Egout part 1
- **Level 7** Egout part 2
- **Level 8** Egout part 3
- **Level 9** Hub (Ferme / Guilde)
- **Level 10** Bataille à la ferme
- **Level 11** Nid du Goblin Lord
- **Level 12** Bataille finale contre le Goblin Lord

### MAP00 - Le Hub (Ferme / Guilde)

#### Présentation
Map composé de deux parties :
- La ferme où vie le Goblin Slayer
- La Guilde des aventuriers, qui se situe dans la ville voisine

#### Structure du niveau

### MAP01 - Cave of Fate

##### Présentation
Map inspirée par le [1er ep](https://www.youtube.com/watch?v=t93tK7Rp0ng) de l'anime, se déroulant dans une grotte

##### Structure du niveau
```mermaid
graph TD
A[START] --- B(Junction)
B --- C(Room)
C --- Z(Secret)
D(ShamanRoom) --- B
E((END)) --- D
```

**Start :** Salle de départ, on va éviter de faire une ouverture directe vers l'extérieur (le joueur pourrait croire que c'est la sortie ou essayer de sortir quoi qu'il arrive, et un mur invisible c'est jamais cool), donc faudrait faire comprendre qu'on vient de dehors mais sans laisser une grosse ouverture, peut etre une corde qui descend dans la grotte et une ouverture en haut qui fait penser qu'on vient de la surface ?

**Junction :** C'est le bout du couloir où les aventuriers se sont fait ouvrir par les goblins. La partie Start -> Junction soit être bien clair et ouverte, alors que la branche Junction -> ShamanRoom doit être un peu caché (comme dans l'anime). Il y a un étendard goblins au centre de la salle.

**Room :** C'est la salle où la prêtresse amène la magicienne empoisonnée, et où le Goblin Slayer apparait réellement pour la 1ere fois dans l'anime. Rien de particulier dans l'anime, il faudra l'agrandir un peu peut être dans le jeu.

**Secret :** Une salle secrète non présente dans l'anime, mais on est dans un mod DOOM donc il faut des salles secrètes. On peut y mettre l'arc, et il faudrait qu'elle soit un peu repérable (c'est la 1ere salle secrète du mod après tout), mais pas trop.

**ShamanRoom :** C'est la salle principale de l'épisode, là d'où le shaman dirige le nid et où les prisonnières sont stockés. C'est une pièce assez ronde avec de mémoire un simili-trone au centre ou au fond, donc parfait pour le miniboss que représente le shaman à ce stade du mod.

**END :** Dans l'anime, c'est en fait une pièce cachée dans l'anime derrière une tapisserie, où sont cachés les bébés goblins que le Goblin Slayer écrase à coups de gourdin. Dans le mod on garde ça, mais on rajoute une grille avec un simili escalier de roches qui monte vers un aperçu de la surface afin de finir le niveau. On peut faire que le portail nécessite une clé, que le Goblin Slayer récupère après avoir tué le shaman.

##### Ennemis du lvl
En tout, le Goblin Slayer [compte **22** goblins](https://youtu.be/t93tK7Rp0ng?t=1217), dont :
- **1** hobgoblin
- **1** shaman
-  **4** bébés

Ce qui fait donc **16** goblins classique.
**7** de ces goblins sont tués par les aventuriers avant l'arrivé du Goblin Slayer. Le nombre d'ennemis sera donc compris entre **15** et **22**

### MAP02 - Tronc Enflammé

##### Présentation
Map inspirée par la fin du [2eme ep](https://youtu.be/HYzfbD7Vu_o?t=953) de l'anime, où l'on voit le Goblin Slayer mettre le feu à un nid de goblins situé entre les racines d'un grand arbre mort.

##### Structure du niveau
```mermaid
graph TD
A[START]
```

##### Ennemis du lvl


##### Éléments interactifs du lvl

## Planning
La suite de l'anime Goblin Slayer devant sortir d'ici Octobre 2020, il serait parfait que le mod sorte en version finale à cette période.
Le planning "idéal" est le suivant :
- **Phase 0** : Fin de l'écriture du cahier des charges **(Décembre 2019)**
- **Phase 1** : [FIRST BUILD] 1ere armes (Epee / Bouclier, Arc Haut Elfe), 1er sort (Protection), Goblins de base + shaman, 1ere map **(Décembre 2019 - Mars 2020)**
- **Phase 2** : [ALPHA] Armes de bases, nouveaux sorts, Goblins phase 1 + hobgoblin, 2eme map + hub si on garde l'idée **(Janvier - Juin 2020)**
- **Phase 3** : [ALPHA 2] Toutes les armes, Tout les sorts, 1er boss (Ogre), 1 map (Ruines Haut Elfe), Goblins phase 2 + Thoulgoblin et Norkgoblin **(Mars - Juillet 2020)**
- **Phase 4** : [BETA] Tout les consommables, nouvelles maps (Egouts), Tout les Goblins, nouveaux boss (Eyeball et Swamp Dragon) **(Juin - Octobre 2020)**
- **Phase 5** : [1.0] Toutes les maps, tout les boss **(Juillet - Fin Octobre 2020)**
- **Phase 6** : Correction bugs, support, compatibilité etc